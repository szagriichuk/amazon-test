rm -rf bin
mkdir bin
find ./src -name *.java > sources_list.txt
javac -d bin -classpath "./lib/*" @sources_list.txt
cp run.sh ./bin
