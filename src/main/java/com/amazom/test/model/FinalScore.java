package com.amazom.test.model;

/**
 * Model class for the {@code FinalScore} abstraction.
 *
 * @author szagriichuk
 */
public class FinalScore {
    private final Student student;
    private final Integer finalScore;

    public FinalScore(Student student, Integer finalScore) {
        this.student = student;
        this.finalScore = finalScore;
    }

    public Student getStudent() {
        return student;
    }

    public Integer getFinalScore() {
        return finalScore;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FinalScore that = (FinalScore) o;

        if (finalScore != null ? !finalScore.equals(that.finalScore) : that.finalScore != null) return false;
        if (student != null ? !student.equals(that.student) : that.student != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = student != null ? student.hashCode() : 0;
        result = 31 * result + (finalScore != null ? finalScore.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "FinalScore{" +
                "student=" + student +
                ", finalScore=" + finalScore +
                '}';
    }
}
