package com.amazom.test.model;

import java.util.Date;

/**
 * Model class for the {@code TestResult} abstraction.
 *
 * @author szagriichuk
 */
public class TestResult {
    private final Date testDate;
    private final Integer score;
    private final Student student;

    public TestResult(Date testDate, Integer score, Student student) {
        this.testDate = testDate;
        this.score = score;
        this.student = student;
    }

    public Date getTestDate() {
        return testDate;
    }

    public Integer getScore() {
        return score;
    }

    public Student getStudent() {
        return student;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TestResult that = (TestResult) o;

        if (score != null ? !score.equals(that.score) : that.score != null) return false;
        if (student != null ? !student.equals(that.student) : that.student != null) return false;
        if (testDate != null ? !testDate.equals(that.testDate) : that.testDate != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = testDate != null ? testDate.hashCode() : 0;
        result = 31 * result + (score != null ? score.hashCode() : 0);
        result = 31 * result + (student != null ? student.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "TestResult{" +
                "testDate=" + testDate +
                ", score=" + score +
                ", student=" + student +
                '}';
    }
}
