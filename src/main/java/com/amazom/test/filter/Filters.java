package com.amazom.test.filter;

import java.util.*;

import static com.amazom.test.conditions.Conditions.checkNotNull;

/**
 * Utility class for filtering functionality.
 * Provides {@code static} method for {@link FilteredSet} and {@link FilteredQueue}.
 * <br>Potentially can be extended for filtering all standard collections from {@code Java Collection Framework}.
 *
 * @author szagriichuk
 */
public final class Filters {
    private Filters() {
    }

    /**
     * Filters input {@code set} using input {@code function}.
     *
     * @return Filtered instance of the input {@link Set}.
     * @throws java.lang.NullPointerException if either of input params is null.
     */
    public static <T> Set<T> filterSet(Set<T> set, FilterFunction<T> function) {
        checkNotNull(set);
        checkNotNull(function);
        return new FilteredSet<T>(set, function);
    }

    /**
     * Filters input {@code queue} using input {@code function}.
     *
     * @return Filtered instance of the input {@link Queue}.
     * @throws java.lang.NullPointerException if either of input params is null.
     */
    public static <T> Queue<T> filterQueue(Queue<T> queue, FilterFunction<T> function) {
        checkNotNull(queue);
        checkNotNull(function);
        return new FilteredQueue<T>(queue, function);
    }

    private static class FilteredSet<T> extends FilteredCollection<T> implements Set<T> {
        public FilteredSet(Set<T> set, FilterFunction<T> function) {
            super(set, function);
            computeSize(iterator());
        }
    }

    private static class FilteredCollection<T> extends AbstractCollection<T> {
        Collection<T> unfilteredSet;
        FilterFunction<T> filterFunction;
        private int size = -1;

        public FilteredCollection(Collection<T> set, FilterFunction<T> function) {
            unfilteredSet = set;
            this.filterFunction = function;
        }

        @Override
        public boolean add(T element) {
            return filterFunction.apply(element) && unfilteredSet.add(element);
        }

        @Override
        public boolean addAll(Collection<? extends T> collection) {
            for (T element : collection) {
                if (!filterFunction.apply(element))
                    return false;
            }
            return unfilteredSet.addAll(collection);
        }


        @Override
        public Iterator<T> iterator() {
            return new Iterator<T>() {
                Iterator<T> unfiltered = unfilteredSet.iterator();

                @Override
                public boolean hasNext() {
                    return unfiltered.hasNext();
                }

                @Override
                public T next() {
                    while (hasNext()) {
                        T data = unfiltered.next();
                        if (filterFunction.apply(data))
                            return data;
                        else remove();
                    }
                    return null;
                }

                @Override
                public void remove() {
                    unfiltered.remove();
                }
            };
        }

        @Override
        public int size() {
            if (size == -1) size = computeSize(iterator());
            return size;
        }

        protected int computeSize(Iterator<T> iterator) {
            int count = 0;
            while (iterator.hasNext()) {
                if (iterator.next() != null) {
                    count++;
                }
            }
            size = count;
            return count;
        }
    }

    private static class FilteredQueue<T> extends FilteredCollection<T> implements Queue<T> {
        Queue<T> queue;

        public FilteredQueue(Queue<T> queue, FilterFunction<T> function) {
            super(queue, function);
            this.queue = queue;
            computeSize(iterator());
        }

        @Override
        public boolean offer(T t) {
            return add(t);
        }

        @Override
        public T remove() {
            T data = queue.remove();
            if (data != null) {
                while (!filterFunction.apply(data)) {
                    data = queue.remove();
                }
            }
            return data;
        }

        @Override
        public T poll() {
            T data = queue.poll();
            if (data != null) {
                while (!filterFunction.apply(data)) {
                    data = queue.poll();
                }
            }
            return data;
        }

        @Override
        public T element() {
            T data = queue.element();
            if (data != null) {
                while (!filterFunction.apply(data)) {
                    data = queue.element();
                }
            }
            return data;
        }

        @Override
        public T peek() {
            T data = queue.peek();
            while (!filterFunction.apply(data)) {
                data = queue.peek();
            }
            return data;
        }
    }
}
