package com.amazom.test.filter;

/**
 * Function for filtering data using {@link com.amazom.test.filter.Filters} functionality.
 *
 * @author szagriichuk
 */
public interface FilterFunction<T> {
    /**
     * Returns the result of applying this function to input {@code data}.
     */
    boolean apply(T data);
}
