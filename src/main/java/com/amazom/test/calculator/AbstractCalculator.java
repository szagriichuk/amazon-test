package com.amazom.test.calculator;

import com.amazom.test.model.FinalScore;
import com.amazom.test.model.Student;
import com.amazom.test.model.TestResult;

import java.util.*;

/**
 * Abstract representation of {@link FinalScoreCalculator}. Contains common methods for generic proposes.
 *
 * @author szagriichuk
 */
public abstract class AbstractCalculator implements FinalScoreCalculator {
    protected static final int SCORES_COUNT = 5;

    protected Integer calculateFinalScore(Iterator<TestResult> value) {
        int result = 0;
        while (value.hasNext()) {
            TestResult testResult = value.next();
            if (testResult != null)
                result += testResult.getScore();
            else break;
        }
        return result / SCORES_COUNT;
    }

    protected PriorityQueue<TestResult> createQueue() {
        PriorityQueue<TestResult> queue;
        queue = new PriorityQueue<TestResult>(SCORES_COUNT, new Comparator<TestResult>() {
            @Override
            public int compare(TestResult o1, TestResult o2) {
                return o2.getScore().compareTo(o1.getScore());
            }
        });
        return queue;
    }

    protected void addToFinalScores(Set<FinalScore> finalScores, Student student, Iterator<TestResult> studentResult) {
        FinalScore finalScore = new FinalScore(student, calculateFinalScore(studentResult));
        finalScores.add(finalScore);
    }

    protected long roundDate(long date) {
        return Math.round(date / 86400000D); // 1000 * 60 * 60 * 24
    }

    protected boolean compareDates(Date resultDate, Date from, Date to) {
        return roundDate(from.getTime()) <= roundDate(resultDate.getTime()) && roundDate(to.getTime()) >= roundDate(resultDate.getTime());
    }
}
