package com.amazom.test.calculator.guava;

import com.amazom.test.calculator.AbstractCalculator;
import com.amazom.test.model.FinalScore;
import com.amazom.test.model.Student;
import com.amazom.test.model.TestResult;
import com.google.common.base.Predicate;

import java.util.*;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterators.filter;
import static com.google.common.collect.Sets.filter;

/**
 * Guava implementation of the {@link com.amazom.test.calculator.FinalScoreCalculator}.
 * That class is using for testing correctness of custom implementation,
 * but would be used as main implementation.
 *
 * @author szagriichuk
 */
public class GuavaFinalScoreCalculator extends AbstractCalculator {
    @Override
    public Set<FinalScore> calculate(Set<TestResult> data, Date from, Date to) {
        checkNotNull(data);
        checkNotNull(from);
        checkNotNull(to);

        Set<TestResult> dateFilteredResult = filter(data, createDateFilterPredicate(from, to));
        Set<Student> students = createUniqueStudentsSet(dateFilteredResult);
        PriorityQueue<TestResult> scoreQueue = createQueue();
        addDataToQueue(scoreQueue, dateFilteredResult);
        return calculateFinalScores(students, scoreQueue);
    }

    private Set<FinalScore> calculateFinalScores(Set<Student> students, PriorityQueue<TestResult> scoreQueue) {
        Set<FinalScore> finalScores = new HashSet<FinalScore>();
        for (final Student student : students) {
            Iterator<TestResult> studentResult = filter(scoreQueue.iterator(), createStudentPredicate(student));
            addToFinalScores(finalScores, student, studentResult);
        }
        return finalScores;
    }

    private Predicate<TestResult> createStudentPredicate(final Student student) {
        return new Predicate<TestResult>() {
            @Override
            public boolean apply(TestResult input) {
                return student.equals(input.getStudent());
            }
        };
    }

    private void addDataToQueue(PriorityQueue<TestResult> scoreQueue, Set<TestResult> dateFilteredResult) {
        for (TestResult testResult : dateFilteredResult) {
            scoreQueue.offer(testResult);
        }
    }

    private Set<Student> createUniqueStudentsSet(Set<TestResult> dateFilteredResult) {
        Set<Student> students = new HashSet<Student>();
        for (TestResult testResult : dateFilteredResult) {
            students.add(testResult.getStudent());
        }
        return students;
    }

    private Predicate<? super TestResult> createDateFilterPredicate(final Date from, final Date to) {
        return new Predicate<TestResult>() {
            @Override
            public boolean apply(TestResult testResult) {
                Date resultDate = testResult.getTestDate();
                return compareDates(resultDate, from, to);
            }
        };
    }
}
