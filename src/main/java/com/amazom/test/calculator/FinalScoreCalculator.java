package com.amazom.test.calculator;

import com.amazom.test.model.FinalScore;
import com.amazom.test.model.TestResult;

import java.util.Date;
import java.util.Set;

/**
 * Main interface for calculation final scores.
 *
 * @author szagriichuk
 */
public interface FinalScoreCalculator {

    /**
     * Calculates and returns set of {@link FinalScore} for inputted date range.<br>
     * A students {@link FinalScore} is calculated as the average of his/her 5 highest {@link TestResult} scores.
     */
    Set<FinalScore> calculate(Set<TestResult> testResults, Date from, Date to);
}
