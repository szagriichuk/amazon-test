package com.amazom.test.calculator;

import com.amazom.test.filter.FilterFunction;
import com.amazom.test.model.FinalScore;
import com.amazom.test.model.Student;
import com.amazom.test.model.TestResult;

import java.util.*;

import static com.amazom.test.conditions.Conditions.checkNotNull;
import static com.amazom.test.filter.Filters.filterSet;

/**
 * Implementation of the {@link FinalScoreCalculator}.
 *
 * @author szagriichuk
 */
public class FinalScoreCalculatorImpl extends AbstractCalculator {

    @Override
    public Set<FinalScore> calculate(Set<TestResult> data, Date from, Date to) {
        checkNotNull(data);
        checkNotNull(from);
        checkNotNull(to);

        Set<TestResult> dateFilteredResult = filterSet(data, createDateFilterFunction(from, to));
        Map<Student, Queue<TestResult>> studentTestResultMap = createStudentsTestResultMap(dateFilteredResult);
        return createFinalScores(studentTestResultMap);
    }

    private Set<FinalScore> createFinalScores(Map<Student, Queue<TestResult>> studentTestResultMap) {
        Set<FinalScore> finalScores = new HashSet<FinalScore>();
        for (Map.Entry<Student, Queue<TestResult>> studentQueueEntry : studentTestResultMap.entrySet()) {
            addToFinalScores(finalScores, studentQueueEntry.getKey(), studentQueueEntry.getValue().iterator());
        }
        return finalScores;
    }

    private Map<Student, Queue<TestResult>> createStudentsTestResultMap(Set<TestResult> dateFilteredResult) {
        Map<Student, Queue<TestResult>> testResultMap = new HashMap<Student, Queue<TestResult>>();
        for (TestResult testResult : dateFilteredResult) {
            Queue<TestResult> results = testResultMap.get(testResult.getStudent());
            if (results == null) {
                results = createAndAddQueueToMap(testResultMap, testResult);
            }
            results.offer(testResult);
        }
        return testResultMap;
    }

    private Queue<TestResult> createAndAddQueueToMap(Map<Student, Queue<TestResult>> testResultMap, TestResult testResult) {
        Queue<TestResult> results = createQueue();
        testResultMap.put(testResult.getStudent(), results);
        return results;
    }

    private FilterFunction<TestResult> createDateFilterFunction(final Date from, final Date to) {
        return new FilterFunction<TestResult>() {
            @Override
            public boolean apply(TestResult data) {
                Date resultDate = data.getTestDate();
                return compareDates(resultDate, from, to);
            }
        };
    }
}
