package com.amazom.test.conditions;

/**
 * Static methods to help code be more readable and correctly.
 *
 * @author szagriichuk
 */
public final class Conditions {
    private Conditions() {
    }

    /**
     * Checks if {@code input} parameter is null. If parameter is null will
     * be thrown {@link java.lang.NullPointerException} with default message.
     */
    public static <T> T checkNotNull(T input) {
        return checkNotNull(input, "Mandatory param could not be null.");
    }

    /**
     * Checks if {@code input} parameter is null. If parameter is null will
     * be thrown {@link java.lang.NullPointerException} with input {@code message}.
     */
    public static <T> T checkNotNull(T input, String message) {
        if (input == null) {
            throw new NullPointerException(message);
        }
        return input;
    }
}
