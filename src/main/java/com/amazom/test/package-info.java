/**
 * Given a list of test results (each with a test date, Student ID, and the students Score),
 * return the Final Score for each student for a user inputted date range.
 * A students Final Score is calculated as the average of his/her 5 highest test scores.
 * You can assume each student has at least 5 test scores.
 */
package com.amazom.test;