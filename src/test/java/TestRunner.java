import com.amazom.test.TestSuite;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

/**
 * Main class for run all tests.
 *
 * @author szagriichuk
 */
public class TestRunner {
    private static final int TEST_RUNNER_COUNT = 100;

    public static void main(String[] args) {
        int count;
        if (args.length == 0 || (count = parseIntParam(args)) < 0)
            count = TEST_RUNNER_COUNT;

        for (int i = 0; i < count; i++) {
            runAllTests();
        }
    }

    private static int parseIntParam(String[] args) {
        try {
            return Integer.parseInt(args[0]);
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    private static void runAllTests() {
        Result result = JUnitCore.runClasses(TestSuite.class);
        java.util.List<Failure> failures = result.getFailures();
        if (failures.isEmpty()) {
            System.out.println("All " + result.getRunCount() + " tests  passed during " + result.getRunTime() + " ms.");
        } else {
            for (Failure failure : result.getFailures()) {
                System.out.println(failure.toString());
            }
        }
    }
}
