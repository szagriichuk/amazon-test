package com.amazom.test.filter;

import org.junit.Test;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static com.amazom.test.filter.Filters.filterSet;
import static org.junit.Assert.assertEquals;

/**
 * Tests for the {@link com.amazom.test.filter.Filters.FilteredSet} class.
 *
 * @author szagriichuk
 */
public class FiltersTestSet {

    @Test(expected = NullPointerException.class)
    public void testFilterSetNullSet() throws Exception {
        filterSet(null, new FilterFunction<Object>() {
            @Override
            public boolean apply(Object data) {
                return false;
            }
        });
    }

    @Test(expected = NullPointerException.class)
    public void testFilterSetNullFunction() throws Exception {
        filterSet(new HashSet<Object>(), null);
    }

    @Test
    public void testFilterSetFalseFunction() throws Exception {
        Set<String> set = createSetWithParams("ONE", "TWO");

        assertEquals(2, set.size());

        set = filterSet(set, new FilterFunction<String>() {
            @Override
            public boolean apply(String data) {
                return false;
            }
        });

        assertEquals(0, set.size());
    }

    @Test
    public void testFilterSetTrueFunction() throws Exception {
        Set<String> set = createSetWithParams("ONE", "TWO");

        assertEquals(2, set.size());

        set = filterSet(set, new FilterFunction<String>() {
            @Override
            public boolean apply(String data) {
                return true;
            }
        });

        assertEquals(2, set.size());
    }

    @Test
    public void testFilterSetEmptySetFalseFunction() throws Exception {
        Set<String> set = createSetWithParams();

        assertEquals(0, set.size());

        set = filterSet(set, new FilterFunction<String>() {
            @Override
            public boolean apply(String data) {
                return true;
            }
        });

        assertEquals(0, set.size());
    }

    @Test
    public void testFilterSetEmptySetTrueFunction() throws Exception {
        Set<String> set = createSetWithParams();

        assertEquals(0, set.size());

        set = filterSet(set, new FilterFunction<String>() {
            @Override
            public boolean apply(String data) {
                return false;
            }
        });

        assertEquals(0, set.size());
    }

    @Test(expected = NullPointerException.class)
    public void testFilterSetNullParam() throws Exception {
        final String word = "ONE";
        Set<String> set = createSetWithParams(word);
        set.add(null);
        filterSet(set, new FilterFunction<String>() {
            @Override
            public boolean apply(String data) {
                return data.equals(word);
            }
        });
    }

    @Test
    public void testFilterSetOneParam() throws Exception {
        final String word = "TWO";
        Set<String> set = createSetWithParams("ONE", word);

        assertEquals(2, set.size());

        set = filterSet(set, new FilterFunction<String>() {
            @Override
            public boolean apply(String data) {
                return data.equals(word);
            }
        });

        assertEquals(1, set.size());
        assertEquals(word, set.iterator().next());

    }

    @Test
    public void testFilterSetInRange() throws Exception {
        final Set<String> set = createSetWithParams(100);
        Set<String> set1 = createSetWithParams(1000);

        assertEquals(100, set.size());
        assertEquals(1000, set1.size());

        set1 = filterSet(set1, new FilterFunction<String>() {
            @Override
            public boolean apply(String data) {
                return set.contains(data);
            }
        });

        assertEquals(100, set1.size());
    }

    @Test
    public void testFilterSetNotInRange() throws Exception {
        final Set<String> set = createSetWithParams(100);
        Set<String> set1 = createSetWithParams(1000);

        assertEquals(100, set.size());
        assertEquals(1000, set1.size());

        set1 = filterSet(set1, new FilterFunction<String>() {
            @Override
            public boolean apply(String data) {
                return !set.contains(data);
            }
        });

        assertEquals(900, set1.size());
    }

    private Set<String> createSetWithParams(int paramCounts) {
        Set<String> results = new HashSet<String>();
        for (int i = 0; i < paramCounts; i++) {
            results.add(String.valueOf(i));
        }
        return results;
    }

    private Set<String> createSetWithParams(String... strings) {
        Set<String> set = new HashSet<String>();
        Collections.addAll(set, strings);
        return set;
    }
}
