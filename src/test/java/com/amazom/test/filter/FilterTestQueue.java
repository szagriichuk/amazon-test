package com.amazom.test.filter;

import org.junit.Test;

import java.util.Collections;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

import static com.amazom.test.filter.Filters.filterQueue;
import static org.junit.Assert.assertEquals;

/**
 * Tests for the {@link com.amazom.test.filter.Filters.FilteredQueue} class.
 *
 * @author szagriichuk
 */
public class FilterTestQueue {

    @Test(expected = NullPointerException.class)
    public void testFilterQueueNullQueue() throws Exception {
        filterQueue(null, new FilterFunction<Object>() {
            @Override
            public boolean apply(Object data) {
                return false;
            }
        });
    }

    @Test(expected = NullPointerException.class)
    public void testFilterQueueNullFunction() throws Exception {
        filterQueue(new LinkedList<Object>(), null);
    }

    @Test
    public void testFilterQueueFalseFunction() throws Exception {
        Queue<String> queue = createQueueWithParams("ONE", "TWO");

        assertEquals(2, queue.size());

        queue = filterQueue(queue, new FilterFunction<String>() {
            @Override
            public boolean apply(String data) {
                return false;
            }
        });

        assertEquals(0, queue.size());
    }

    @Test
    public void testFilterQueueTrueFunction() throws Exception {
        Queue<String> queue = createQueueWithParams("ONE", "TWO");

        assertEquals(2, queue.size());

        queue = filterQueue(queue, new FilterFunction<String>() {
            @Override
            public boolean apply(String data) {
                return true;
            }
        });

        assertEquals(2, queue.size());
    }

    @Test
    public void testFilterQueueEmptyQueueFalseFunction() throws Exception {
        Queue<String> queue = createQueueWithParams();

        assertEquals(0, queue.size());

        queue = filterQueue(queue, new FilterFunction<String>() {
            @Override
            public boolean apply(String data) {
                return false;
            }
        });

        assertEquals(0, queue.size());
    }

    @Test
    public void testFilterQueueEmptyQueueTrueFunction() throws Exception {
        Queue<String> queue = createQueueWithParams();

        assertEquals(0, queue.size());

        queue = filterQueue(queue, new FilterFunction<String>() {
            @Override
            public boolean apply(String data) {
                return true;
            }
        });

        assertEquals(0, queue.size());
    }

    @Test(expected = NullPointerException.class)
    public void testFilterQueueNullParam() throws Exception {
        final String word = "ONE";
        Queue<String> queue = createQueueWithParams(word);
        queue.offer(null);
        filterQueue(queue, new FilterFunction<String>() {
            @Override
            public boolean apply(String data) {
                return data.equals(word);
            }
        });
    }

    @Test
    public void testFilterQueueOneParam() throws Exception {
        final String word = "TWO";
        Queue<String> queue = createQueueWithParams("ONE", word);

        assertEquals(2, queue.size());

        queue = filterQueue(queue, new FilterFunction<String>() {
            @Override
            public boolean apply(String data) {
                return data.equals(word);
            }
        });

        assertEquals(1, queue.size());
        assertEquals(word, queue.iterator().next());
    }

    @Test
    public void testFilterQueueInRange() throws Exception {
        final Queue<String> queue = createQueueWithParams(100);
        Queue<String> queue1 = createQueueWithParams(1000);

        assertEquals(100, queue.size());
        assertEquals(1000, queue1.size());

        queue1 = filterQueue(queue1, new FilterFunction<String>() {
            @Override
            public boolean apply(String data) {
                return queue.contains(data);
            }
        });

        assertEquals(100, queue1.size());
    }

    @Test
    public void testFilterQueueNotInRange() throws Exception {
        final Queue<String> queue = createQueueWithParams(100);
        Queue<String> queue1 = createQueueWithParams(1000);

        assertEquals(100, queue.size());
        assertEquals(1000, queue1.size());

        queue1 = filterQueue(queue1, new FilterFunction<String>() {
            @Override
            public boolean apply(String data) {
                return !queue.contains(data);
            }
        });

        assertEquals(900, queue1.size());
    }

    private Queue<String> createQueueWithParams(int paramCounts) {
        Queue<String> results = new PriorityQueue<String>();
        for (int i = 0; i < paramCounts; i++) {
            results.add(String.valueOf(i));
        }
        return results;
    }

    private Queue<String> createQueueWithParams(String... strings) {
        Queue<String> queue = new LinkedList<String>();
        Collections.addAll(queue, strings);
        return queue;
    }
}
