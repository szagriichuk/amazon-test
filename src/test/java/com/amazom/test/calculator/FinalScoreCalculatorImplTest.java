package com.amazom.test.calculator;

import com.amazom.test.model.FinalScore;
import com.amazom.test.model.Student;
import com.amazom.test.model.TestResult;
import junit.framework.Assert;
import org.junit.Test;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Tests for the {@link FinalScoreCalculatorImpl} class.
 *
 * @author szagriichuk
 */
public class FinalScoreCalculatorImplTest extends CommonCalculatorTest {

    @Test(expected = NullPointerException.class)
    public void testCalculateWithNullTestResultList() throws Exception {
        FinalScoreCalculator finalScoreCalculator = new FinalScoreCalculatorImpl();
        finalScoreCalculator.calculate(null, new Date(), new Date());
    }

    @Test(expected = NullPointerException.class)
    public void testCalculateWithNullDateFrom() throws Exception {
        FinalScoreCalculator finalScoreCalculator = new FinalScoreCalculatorImpl();
        finalScoreCalculator.calculate(new HashSet<TestResult>(), null, new Date());
    }

    @Test(expected = NullPointerException.class)
    public void testCalculateWithNullDateTo() throws Exception {
        FinalScoreCalculator finalScoreCalculator = new FinalScoreCalculatorImpl();
        finalScoreCalculator.calculate(new HashSet<TestResult>(), new Date(), null);
    }


    @Test
    public void testCalculateWithSameDateFromAndTo() throws Exception {
        FinalScoreCalculator finalScoreCalculator = new FinalScoreCalculatorImpl();
        Set<TestResult> testResultSet = createTestResults();
        Set<FinalScore> finalScores = finalScoreCalculator.calculate(testResultSet, createDate(2014, 3, 24), createDate(2014, 3, 24));

        Assert.assertNotNull(finalScores);
        Assert.assertEquals(1, finalScores.size());

        FinalScore finalScore = getElementByIndexFromSet(finalScores, 0);

        Assert.assertNotNull(finalScore);
        Assert.assertEquals(11 / 5, finalScore.getFinalScore().intValue());
    }

    @Test
    public void testCalculateWithOneDayRange() throws Exception {
        FinalScoreCalculator finalScoreCalculator = new FinalScoreCalculatorImpl();
        Set<TestResult> testResultSet = createTestResults();
        Set<FinalScore> finalScores = finalScoreCalculator.calculate(testResultSet, createDate(2014, 3, 24), createDate(2014, 3, 25));

        Assert.assertNotNull(finalScores);
        Assert.assertEquals(1, finalScores.size());

        FinalScore finalScore = getElementByIndexFromSet(finalScores, 0);

        Assert.assertNotNull(finalScore);
        Assert.assertEquals((11 + 11) / 5, finalScore.getFinalScore().intValue());
    }

    @Test
    public void testCalculateWith5DaysRange() throws Exception {
        FinalScoreCalculator finalScoreCalculator = new FinalScoreCalculatorImpl();
        Set<TestResult> testResultSet = createTestResults();
        Set<FinalScore> finalScores = finalScoreCalculator.calculate(testResultSet, createDate(2014, 3, 21), createDate(2014, 3, 25));

        Assert.assertNotNull(finalScores);
        Assert.assertEquals(1, finalScores.size());

        FinalScore finalScore = getElementByIndexFromSet(finalScores, 0);

        Assert.assertNotNull(finalScore);
        Assert.assertEquals(11, finalScore.getFinalScore().intValue());
    }

    @Test
    public void testCalculateWithOldestDate() throws Exception {
        FinalScoreCalculator finalScoreCalculator = new FinalScoreCalculatorImpl();
        Set<TestResult> testResultSet = createTestResults();
        Set<FinalScore> finalScores = finalScoreCalculator.calculate(testResultSet, createDate(2014, 2, 21), createDate(2014, 2, 25));

        Assert.assertNotNull(finalScores);
        Assert.assertEquals(0, finalScores.size());
    }

    @Test
    public void testCalculateWithNewestDate() throws Exception {
        FinalScoreCalculator finalScoreCalculator = new FinalScoreCalculatorImpl();
        Set<TestResult> testResultSet = createTestResults();
        Set<FinalScore> finalScores = finalScoreCalculator.calculate(testResultSet, createDate(2014, 4, 21), createDate(2014, 4, 25));

        Assert.assertNotNull(finalScores);
        Assert.assertEquals(0, finalScores.size());
    }

    @Test
    public void testCalculateWithRandomData() throws Exception {
        FinalScoreCalculator finalScoreCalculator = new FinalScoreCalculatorImpl();
        Set<TestResult> testResultSet = createRandomTestResults(1000, 3, 20, 5);
        Set<FinalScore> finalScores = finalScoreCalculator.calculate(testResultSet, createDate(2014, 3, 21), createDate(2014, 6, 21));

        Assert.assertNotNull(finalScores);
        Assert.assertEquals(5, finalScores.size());
    }

    private Set<TestResult> createTestResults() {
        return new HashSet<TestResult>() {{
            add(new TestResult(createDate(2014, 3, 19), 11, new Student(String.valueOf(1))));
            add(new TestResult(createDate(2014, 3, 20), 11, new Student(String.valueOf(1))));
            add(new TestResult(createDate(2014, 3, 21), 11, new Student(String.valueOf(1))));
            add(new TestResult(createDate(2014, 3, 22), 12, new Student(String.valueOf(1))));
            add(new TestResult(createDate(2014, 3, 23), 11, new Student(String.valueOf(1))));
            add(new TestResult(createDate(2014, 3, 24), 11, new Student(String.valueOf(1))));
            add(new TestResult(createDate(2014, 3, 25), 11, new Student(String.valueOf(1))));
        }};
    }
}
