package com.amazom.test.calculator;

import com.amazom.test.model.Student;
import com.amazom.test.model.TestResult;

import java.util.*;

/**
 * @author szagriichuk
 */
public class CommonCalculatorTest {

    protected Set<TestResult> createRandomTestResults(int count, int startMonth, int startDay, int studentIdSeed) {
        Set<TestResult> results = new HashSet<TestResult>();
        Random random = new Random();
        for (int i = 0; i < count; i++) {
            results.add(new TestResult(createDate(2014, startMonth, startDay + i), random.nextInt(12), new Student(String.valueOf(random.nextInt(studentIdSeed)))));
        }
        return results;
    }

    protected Date createDate(int year, int month, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day, 0, 0, 0);
        return calendar.getTime();
    }

    protected <T> T getElementByIndexFromSet(Set<T> set, int index) {
        Iterator<T> setIterator = set.iterator();
        while (setIterator.hasNext() && index > 0) {
            setIterator.next();
            index--;
        }
        return setIterator.next();
    }
}
