package com.amazom.test.calculator;

import com.amazom.test.calculator.guava.GuavaFinalScoreCalculator;
import com.amazom.test.model.FinalScore;
import com.amazom.test.model.TestResult;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;
import java.util.Set;

/**
 * @author szagriichuk
 */
public class ComparingFinalScoreCalculatorTest extends CommonCalculatorTest {
    private GuavaFinalScoreCalculator calculator = new GuavaFinalScoreCalculator();
    private FinalScoreCalculator finalScoreCalculator = new FinalScoreCalculatorImpl();

    @Test
    public void testCalculate() throws Exception {
        Set<TestResult> testResults = createRandomTestResults(1000, 1, 1, 5);
        Date fromDate = createDate(2014, 2, 1);
        Date toDate = createDate(2014, 3, 31);
        Set<FinalScore> scores = calculator.calculate(testResults, fromDate, toDate);
        Set<FinalScore> scores1 = finalScoreCalculator.calculate(testResults, fromDate, toDate);
        Assert.assertEquals(scores, scores1);
    }
}
