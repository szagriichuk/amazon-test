package com.amazom.test.conditions;

import org.junit.Test;

import static com.amazom.test.conditions.Conditions.checkNotNull;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.fail;

/**
 * Tests for the {@link Conditions} class.
 *
 * @author szagriichuk
 */
public class ConditionsTest {

    @Test(expected = NullPointerException.class)
    public void testCheckNotNullWithNullParam() throws Exception {
        checkNotNull(null);
    }

    @Test
    public void testCheckNotNullWithNotNullParam() throws Exception {
        Object obj = new Object();
        Object obj1 = checkNotNull(obj);
        assertEquals(obj, obj1);
    }

    @Test(expected = NullPointerException.class)
    public void testCheckNotNullWithNullParamErrorMessage() throws Exception {
        String message = "Error message";
        try {
            checkNotNull(null, message);
        } catch (Exception e) {
            assertEquals(message, e.getMessage());
            throw e;
        }
        fail();
    }

    @Test
    public void testCheckNotNullWithNotNullParamErrorMessage() throws Exception {
        Object obj = new Object();
        String message = "Error message";
        Object obj1 = null;
        try {
            obj1 = checkNotNull(obj, message);
        } catch (Exception e) {
            fail();
        }
        assertEquals(obj, obj1);
    }
}
