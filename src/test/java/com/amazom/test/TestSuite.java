package com.amazom.test;

import com.amazom.test.calculator.ComparingFinalScoreCalculatorTest;
import com.amazom.test.calculator.FinalScoreCalculatorImplTest;
import com.amazom.test.conditions.ConditionsTest;
import com.amazom.test.filter.FilterTestQueue;
import com.amazom.test.filter.FiltersTestSet;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Test suite which contains all tests classes.
 *
 * @author szagriichuk
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({FiltersTestSet.class, FilterTestQueue.class, ConditionsTest.class, FinalScoreCalculatorImplTest.class, ComparingFinalScoreCalculatorTest.class})
public class TestSuite {
}
